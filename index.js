const express = require("express");
// Load HTTP module
const app = express();

const port = 4000;

app.use(express.json()); // allows you app to read json data
app.use(express.urlencoded({extended : true})); //allows your app to read data from forms

app.get("/", (req, res) => {
	res.send("Hello World");
})

app.get("/result",(req, res) => {

	fetch("https://gaffud-simple-express-api.herokuapp.com/result")
	.then(res => res.json())
	.then(data => {

	     res.send(data);
	                
	 });


})

app.listen(port, () => console.log(`Server running at port ${port}`))